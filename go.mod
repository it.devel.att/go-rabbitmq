module go_rabbit

go 1.14

require (
	github.com/sirupsen/logrus v1.7.0
	github.com/streadway/amqp v1.0.0
)
