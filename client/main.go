package main

import (
	"log"
	"os"
	"time"

	"github.com/streadway/amqp"
)

func main() {
	for {
		log.Println("Start worker")

		connectionString := os.Getenv("BROKER_CONNECTION_STRING")
		conn, err := amqp.Dial(connectionString)
		if err != nil {
			log.Printf("[main] amqp.Dial Error: %v", err)
			time.Sleep(time.Second)
			continue
		}
		notify := conn.NotifyClose(make(chan *amqp.Error))

		ch, err := conn.Channel()
		if err != nil {
			log.Printf("[main] conn.Channel Error: %v", err)
			time.Sleep(time.Second)
			continue
		}

		q, err := ch.QueueDeclare(
			"hello", // name
			true,    // durable
			false,   // delete when unused
			false,   // exclusive
			false,   // no-wait
			nil,     // arguments
		)
		if err != nil {
			log.Printf("[main] ch.QueueDeclare Error: %v", err)
			time.Sleep(time.Second)
			continue
		}

		err = ch.Qos(
			1,     // prefetch count
			0,     // prefetch size
			false, // global
		)

		if err != nil {
			log.Printf("[main] ch.Qos Error: %v", err)
			time.Sleep(time.Second)
			continue
		}

		msgs, err := ch.Consume(
			q.Name, // queue
			"",     // consumer
			false,  // auto-ack
			false,  // exclusive
			false,  // no-local
			false,  // no-wait
			nil,    // args
		)
		if err != nil {
			log.Printf("[main] ch.Consume Error: %v", err)
			time.Sleep(time.Second)
			continue
		}

	readMessagesLoop:
		for {
			select {
			case err := <-notify:
				log.Printf("[main] NotifyClose: %v", err)
				time.Sleep(time.Second)
				break readMessagesLoop
			case d := <-msgs:
				log.Printf("Received a message: %s", d.Body)
				time.Sleep(time.Second * 5)

				if err := d.Ack(false); err != nil {
					log.Printf("Error while ACK. Error: %v. Body: %s", err, d.Body)
					time.Sleep(time.Second)
					break readMessagesLoop
				} else {
					log.Printf("Done a message: %s", d.Body)
				}
			}
		}
		log.Println("After break readMessagesLoop!")

		if !conn.IsClosed() {
			if err := ch.Close(); err != nil {
				log.Printf("[main] ch.Close Error: %v", err)
			}

			if err := conn.Close(); err != nil {
				log.Printf("[main] conn.Close Error: %v", err)
			}

		}

		log.Println("Restart worker")
	}
}
