FROM golang:1.14.15-alpine AS builder
# Layer for build binary file

RUN apk add git gcc
WORKDIR /src
COPY go.mod go.sum ./
RUN go mod download

ENV CGO_ENABLED=0
ENV GOOS=linux
ARG APP_VERSION="dev"

COPY . .
RUN go build -i -o /producer main.go
RUN go build -i -o /consumer client/main.go

FROM scratch

COPY --from=builder /producer /producer
COPY --from=builder /consumer /consumer
